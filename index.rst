

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/news/rss.xml>`_


.. _liens_web:

=====================
Web news
=====================


.. toctree::
   :maxdepth: 6

   articles/articles
   history/history
