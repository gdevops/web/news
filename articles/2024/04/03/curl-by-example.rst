.. index::
   pair: curl; Curl by example: Interactive guide (Anton Zhiyanov, 2024-04-03)


.. _zhiyanov_2024_04_03:

======================================================================
2024-04-03 **Curl by example: Interactive guide** by Anton Zhiyanov
======================================================================

- https://antonz.org/curl-by-example/
- https://daniel.haxx.se/

.. youtube:: V5vZWHP-RqU

**Curl (client for URLs)** is a tool for client-side internet transfers (uploads
and downloads) using a specific protocol (such as HTTP, FTP or IMAP), where
the endpoint is identified by a URL.

Curl runs on 92 operating systems and has over 20 billion installations worldwide.

Curl has extensive reference documentation and even a 500-page book devoted
entirely to it. I wanted something lighter, so I made this interactive step-by-step
guide to essential curl operations.

You can read it from start to finish to (hopefully) learn more about curl,
or jump to a specific use case that interests you.

This guide is partially based on the 3.5-hour workshop Mastering the curl
command line by `Daniel Stenberg <https://daniel.haxx.se/>`_, the author of curl.
