.. index::
   ! unmodernweb

.. _katrina_2024_04_08:

================================================
2024-04-08 **unmodernweb.com** by Katrina Grace
================================================

- https://unmodernweb.com/
- https://github.com/kgscialdone
- https://kgscialdone.substack.com/p/web-components-arent-framework-components


What does "unmodern" mean?
============================

unmodern
    unmodern (n.)
    not modern, old-fashioned

The idea behind the Unmodern Web is to create and use modern tools alongside
the philosophy and mindset of the early web.
Rather than follow the current modern philosphies of web design, such as heavy
JavaScript frameworks and single-page applications, unmodern websites strive
for simplicity and straightforwardness by working alongside the way the web
was originally designed.

This is not, however, to say that unmodern websites avoid modern features.
In fact, quite the contrary — unmodern sites are encouraged to employ cutting-edge
CSS and JavaScript where it has sufficient browser support.

What matters is the underlying design philosophy, which stresses straightforward
implementations built on basic browser technologies and the use of libraries
that extend those technologies rather than override them.


I started Unmodern Web as an outlet for my thoughts about web technology and
how best to do web development, but it very quickly evolved into something
of a broader philosophy that I'd like to encourage in the web development
community, heavily inspired by things like HATEOS and The Grug Brained Developer.

The name was somewhat of a random flash of insight — I personally feel that
the unusual wording quite neatly captures the idea of "modern technology
with old-fashioned principles".

Thus, this site serves both as a landing page for my own personal blog and
projects, as well as a way to promote other projects I feel embody the
principle of the Unmodern Web.


https://unmodernweb.com/projects
===================================

HTMX
------

- :ref:`tuto_htmx:htmx_tuto`

hyperscript
-----------

- :ref:`tuto_hyperscript:hyperscript`
