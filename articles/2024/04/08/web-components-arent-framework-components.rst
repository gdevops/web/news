.. index::
   pair: Web components; facet
   ! facet

.. _grace_2024-04-08:

========================================================================================
2024-04-08 **Web Components Aren't Framework Components, facet** by Katrina Grace
========================================================================================

- https://kgscialdone.substack.com/p/web-components-arent-framework-components
- https://github.com/kgscialdone/facet

Web components, on the other hand - **or custom elements, as they should properly
be called - are new HTML elements** with custom behavior attached.

Rather than replacing your HTML by reorganizing it into chunks, custom elements
work alongside your existing HTML to encapsulate and simplify localized behaviors.

This allows them to be dropped trivially into any page without wildly restructuring
your site around them, since once they’ve been defined, they act exactly like
normal HTML elements - you just get to define the special behavior they represent.
