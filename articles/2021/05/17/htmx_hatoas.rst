

.. _htmx_2021_05_17:

==================================================
2021-05-17 **htmx is a back to basics movement**
==================================================

- https://x.com/htmx_org/status/1394026682609127426?s=20


htmx is a back to basics movement

the theory is that HTML stopped making progress as a hypermedia (hypermedium?)
and we should resume progress and see how far we can get with UX improvements
within the (real, original) REST-ful/HATEOAS network model
