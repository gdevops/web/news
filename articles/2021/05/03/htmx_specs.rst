

.. _htmx_2021_05_03:

=============================================================
2021-05-03 **htmx specs, extending-html-as-a-hypermedia**
=============================================================

- https://discourse.wicg.io/
- https://whatwg.org/working-mode#new-proposals
- https://discord.com/channels/725789699527933952/725789747212976259/838841328061251653
- https://discourse.wicg.io/t/extending-html-as-a-hypermedia/5294


Proposition
=============

.. figure:: htmx_specs.png
   :align: center


have you thought about writing a proposal for WHATWG ? I do agree with
the idea that htmx should not exist as a library but rather be part of
the HTML spec.

I think this idea will resonate with a lot of people once it's framed as
HTML 6 o 7 o 10...

Makes a lot of sense and the HTMX library might work as a proof of
concept of how it could potentially work.

I believe there are enough use cases for these features and a simple
incremental change to the spec to make it work,

::

    [20:46]
    It's worth the try, they do have a very open and documented process
    which seem low effort at least to present a feature idea
    [20:48]
    https://participate.whatwg.org/
    [20:49]
    https://whatwg.org/working-mode#new-proposals

here's also WICG https://discourse.wicg.io/ for informal discussions on specs


Annonce twitter
================

- https://x.com/htmx_org/status/1389320281496309768?s=20
- https://discourse.wicg.io/t/extending-html-as-a-hypermedia/5294
- https://x.com/yoavweiss
- https://x.com/TravisLeithead

i created a discussion on web incubator cg regarding moving HTML forward as a hypermedia:

https://discourse.wicg.io/t/extending-html-as-a-hypermedia/5294

http://htmx.org probably shouldn't exist, rather it should be how HTML works

/cc @TravisLeithead @yoavweiss


Problem
=============

HTML has advanced in many ways over the last two decades, but it hasn’t
grown much as a hypermedia.

In particular, anchors and forms are still the primary way to drive
network activity via user interactions in pure HTML-based applications.

Due to these limitations, javascript has come to the fore as the premier
technology for building advanced web applications.

Unfortunately, as a consequence, much of the original power and simplicity
of the web is being lost.
In particular, Hypermedia is no longer The Engine of Application State.

Rather, application state is often maintained in in memory javascript
stores and the DOM is reactively updated based on that state.

A Potential Way Forward
==========================

I have been working on an open source javascript library for about a
decade now that tries to address the shortcomings of HTML while still
remaining firmly within the original, REST-ful model of the web.
It began as intercooler.js 4 and last summer was renamed to htmx 19,
after I broke the dependency on jQuery.

The core idea is to use extended HTML attributes to drive server
interactions, and allow for the replacement of elements within the DOM,
rather than whole-page refreshes, based on HTML content returned from
the server.

This very much satisfies the REST constraints, and extends HTML (hence htmx).

htmx demonstrates that, with a few declarative HTML attributes, you can
express many common UX patterns that would normally be done with javascript.

The most significant attributes are:

- hx-get 7, etc. - specifies the url to issue a request too
- hx-trigger 3 - specifies the event that triggers a request
- hx-target - specifies the element to update in the DOM
- hx-swap 2 - specifies the manner in which the returned content should be swapped into the DOM

Here is an example that demonstrates the “Live Search” pattern, using
only a few attributes:

- https://htmx.org/examples/active-search/ 40

Moving HTML Forward as a Hypertext
=======================================

It seems to me that much of the functionality of htmx could, and perhaps
should, simply be part of normal HTML.

There is no reason a hypermedia should have to replace the entire document
on navigation, and there is no reason why only clicks and submits should
be able to drive network interactions.

The particulars of the htmx attributes don’t matter nearly as much as
the general idea of revisiting HTML qua hypermedia and thinking about
how we can drive it, and, therefore hypermedia-based application development,
forward.


Discord discussion
====================

- https://discord.com/channels/725789699527933952/725789747212976259/839098380466257930

htmx as official spec ?
    Note that "htmx as spec" doesn't mean copying and pasting the whole htmx
    documentation into html.spec.whatwg.org right away, rather extending
    the scope of existing html features in ways that aid hypertext application
    development (see my post on the wicg topic). htmx and the like would
    continue existing as a place for new ideas to be tested in the real world.

    At some time in the future, all these libraries will likely be obsolete
    for new projects, but when that will happen will be determined by developers,
    not the spec.

Ten years later we will know what got used and what not.
    but that doesn't mean we should wait ten years until starting discussions
    about improving html itself. Spec conversations can go on as
    htmx/unpoly/hotwire/other development continues and new libraries spring up.
    These discussions can also give a view of what the general web developer
    community thinks of html-driven development, which will help htmx
