

.. _htmx_2021_03_18:

========================================================
Creating a Github profile search component in htmx
========================================================

.. seealso::

   - https://dev.to/rajasegar/creating-a-github-profile-search-component-in-htmx-1pl5
   - https://github.com/rajasegar/htmx-github-search
   - https://htmx-github-search.herokuapp.com/




Introduction
============


In this post we are going to take a look at creating a simple Github
profile search component using htmx.


What is htmx ?
=================

**htmx** allows you to build modern user interfaces with the simplicity
and  the power of hypertext.

It lets you to access AJAX, CSS Transitions, WebSockets and Server Sent Events
directly in HTML, using attributes.

It is small ~9KB (minified and gzipped), dependency-free, extendable and IE11 compatible.
