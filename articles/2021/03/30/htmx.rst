

.. _htmx_news_2021_03_30:

====================================================================================================
2021-03-30 Htmx & The Future of Web Dev, Part 1, 26 Mar 2021 + html-over-the-wire.herokuapp.com
====================================================================================================





htmx-the-future-of-web
========================

.. seealso::

   - https://mekhami.github.io/2021/03/26/htmx-the-future-of-web/


Back in the day, the web was very simple.

In this series, we’ll explore a tiny library that can bring back yesteryear’s
simplicity to the modern web, without sacrificing the parts that make
the modern web feel better to use.

The web used to be pretty straight forward. URLs pointed to servers,
servers mashed up their data into html, and browsers rendered that response.

What a glorious era that was. Hypertext was all we needed. Simple frameworks
popped up around this simple paradigm, and allowed developers to go from
spending months on basic functionalities to spending hours to create
relatively complex projects.

More time was spent on the business logic, on application design, instead
of spending untold man hours wiring up scripts to databases and concatenating
strings of html together.

I’m not going to tell you we should have stopped there.
The web grew, and evolved, obviously, and we needed javascript for things.


https://html-over-the-wire.herokuapp.com/
===========================================

.. seealso::

   - https://html-over-the-wire.herokuapp.com/
   - https://github.com/rajasegar/html_over_the_wire


.. figure:: comparison/html_over.png
   :align: center

   https://html-over-the-wire.herokuapp.com/


rajasegar — Aujourd’hui à 09:52

Built a comparison site for different HTML over the wire solutions
https://html-over-the-wire.herokuapp.com/

Planning to boost with htmx and other upgrades, any comments / feedback appreciated
