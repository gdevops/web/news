

.. _html_2021_03_19:

========================================================
The Future of Web Software Is HTML-over-WebSocket
========================================================

.. seealso::

   - https://alistapart.com/article/the-future-of-web-software-is-html-over-websockets/
   - https://medium.com/@minthesize/the-post-modern-web-stimulusreflex-or-the-renaissance-of-simplicity-9b0357738814
   - https://alistapart.com/author/matt-e-patterson/
   - https://x.com/mepatterson
   - https://x.com/stimulusreflex
   - https://github.com/jonathan-s/django-sockpuppet





Introduction
=============


The dual approach of marrying a Single Page App with an API service has
left many dev teams mired in endless JSON wrangling and state discrepancy
bugs across two layers.
This costs dev time, slows release cycles, and saps the bandwidth for innovation.

But a new WebSockets-driven approach is catching web developers’ attention.

One that reaffirms the promises of classic server-rendered frameworks:
fast prototyping, server-side state management, solid rendering performance,
rapid feature development, and straightforward SEO.

One that enables multi-user collaboration and reactive, responsive designs
without building two separate apps.

The end result is a single-repo application that feels to users just as
responsive as a client-side all-JavaScript affair, but with straightforward
templating and far fewer loading spinners, and no state misalignments,
since state only lives in one place.

All of this sets us up for a considerably easier (and faster!)
development path.


But then the JavaScript everything notion took hold…
=========================================================

As the web development world pushed deeper into the 2010s, the tides began
to turn, and server-rendered frameworks took something of a backseat
to the Single Page Application, wholly built in JavaScript and run
entirely on the client’s computer.

At many companies, the “server” became relegated to hosting an API data
service only, with most of the business logic and all of the HTML rendering
happening on the client, courtesy of the big ’ol package of JavaScript
that visitors were forced to download when they first hit the site.

**This is where things started to get ugly.**


Fast-forward to 2020 and the web isn’t getting any faster, as we were
promised it would with SPAs.

Shoving megabytes of JavaScript down an iPhone 4’s throat doesn’t make
for a great user experience.
And if you thought building a professional web app took serious resources,
what about building a web app and an API service and a communication layer
between them? Do we really believe that every one of our users is going
to have a device capable of digesting 100 kB of JSON and rendering a
complicated HTML table faster than a server-side app could on even
a mid-grade server ?

We’re not building web software for us. We’re building it for **them**.

The users of our software have expectations of how it’s going to work
for them.
We have to meet them where they are.

Our users are no longer excited about full-page refreshes and ugly Rube
Goldberg-ian multi-form workflows.

The SPA approach was the next logical leap from piles of unorganized
spaghetti JavaScript living on the server.
The problem, though: it was a 5% improvement, not a 500% improvement.
